package com.shemnon.weld;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.shemnon.weld.Proto.BlockHeader;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.IntStream;

/**
 *
 * Created by shemnon on 12 Jun 2014.
 */
public class SimpleTest {

    public BlockHeader getGenesisBlock() {
        BlockHeader.Builder partial = BlockHeader.newBuilder()
            .setVersion(0)
            .setDifficulty(getDifficulties()[0])
            .setNonce(0x1eafdecafbadcafeL);
        try {
            MessageDigest md = MessageDigest.getInstance(getHashAlgo());
            partial.setHash(ByteString.copyFrom(md.digest(partial.build().toByteArray())));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return partial.build();
    }

    public String getHashAlgo() {
        return "SHA-256";
    }

    public String getDSAAlgo() {
        return "SHA1withECDSA";
    }

    public String getDSAKeyType() {
        return "EC";
    }

    public double[] getDifficulties() {
        return new double[] {0x2P245, 0x2P240, 0x2P235};
    }

    KeyPair keyPair;

    public KeyPair getKeyPair() {
        try {
            if (keyPair == null) {
                KeyPairGenerator keyGen = KeyPairGenerator.getInstance(getDSAKeyType());
                SecureRandom random = SecureRandom.getInstance("SHA1PRNG");

                keyGen.initialize(256, random);

                keyPair = keyGen.generateKeyPair();
            }
            return keyPair;
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    public void run() throws InvalidKeySpecException, InvalidProtocolBufferException,
        NoSuchAlgorithmException, InvalidKeyException, SignatureException,
        UnsupportedEncodingException {

        MessageDigest md = MessageDigest.getInstance(getHashAlgo());

        KeyPair pair = getKeyPair();
        assert pair != null;

        BlockHeader prevHeader =
            signHeader(pair, getGenesisBlock().toBuilder()
            .setPayload(ByteString.copyFromUtf8("Sample payload hash, not essential to demo."))
            .addAllSigningPublicKey(Collections.singletonList(ByteString.copyFrom(pair.getPublic().getEncoded())))
            .build());
        System.out.println(prevHeader);

        double[] difficulties = getDifficulties();

        ByteString[] prevHashes = new ByteString[difficulties.length];
        Arrays.fill(prevHashes, prevHeader.getHash());

        //noinspection InfiniteLoopStatement
        while (true) {
            ByteString prevHash = prevHeader.getHash();
            double hashAsDouble = new BigInteger(prevHash.toByteArray()).doubleValue();
            Map<Integer, ByteString> prevHashsMap = new HashMap<>();

            for (int i = 0; i < difficulties.length; i++) {
                if (hashAsDouble < difficulties[i]) {
                    prevHashes[i] = prevHash;
                }
                prevHashsMap.put(i, prevHashes[i]);
            }

            BlockHeader thisHeader = BlockHeader.newBuilder(prevHeader)
                .putAllPreviousHash(prevHashsMap)
                .setTime(System.currentTimeMillis() / 1000)
                .clearSignature()
                .clearHash()
                .build();

            prevHeader = signHeader(pair, thisHeader);
            System.out.println(prevHeader);
            if (!verifyBlock(prevHeader.toByteArray())) {
                throw new RuntimeException("Failed Verification");
            }

            //TODO assert no repeated R value.
        }
    }

    public static void main(String... arg) throws NoSuchAlgorithmException, InvalidKeyException,
        UnsupportedEncodingException, SignatureException, InvalidKeySpecException,
        InvalidProtocolBufferException {

        new SimpleTest().run();
    }

    BlockHeader signHeader(KeyPair pair, BlockHeader oHeader) throws
        NoSuchAlgorithmException,
        InvalidKeyException, UnsupportedEncodingException, SignatureException {

        Optional<BlockHeader> blockOptional;

        do {
            blockOptional =
                IntStream.range(1, 1 << 20)
//                    .parallel()
                    .mapToObj(l -> {
                    //new Random().ints().parallel().mapToObj(l -> {
                    //Stream.generate(nonce::incrementAndGet).map(l -> {
                    try {
                        MessageDigest md = MessageDigest.getInstance(getHashAlgo());
                        BlockHeader.Builder headerBuilder = oHeader.toBuilder();
                        headerBuilder.setTime(System.currentTimeMillis() / 1000);
                        headerBuilder.setNonce(l);

                        PrivateKey priv = pair.getPrivate();

                        Signature dsa = Signature.getInstance(getDSAAlgo());

                        dsa.initSign(priv);

                        dsa.update(headerBuilder.build().toByteArray());
                        byte[] sig = dsa.sign();
                        headerBuilder.setSignature(ByteString.copyFrom(sig));

                        md.reset();
                        byte[] hash = md.digest(headerBuilder.build().toByteArray());
                        return headerBuilder.setHash(ByteString.copyFrom(hash)).build();
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                }).filter(bh -> {
                    double hashAsDouble = new BigInteger(bh.getHash().toByteArray()).doubleValue();
                    return hashAsDouble < bh.getDifficulty() && hashAsDouble > 0;
                }).findFirst();
        } while (!blockOptional.isPresent());

        return blockOptional.get();
    }

    boolean verifyBlock(byte[] blockAsBytes) throws NoSuchAlgorithmException,
        InvalidKeySpecException, InvalidKeyException, SignatureException,
        InvalidProtocolBufferException {

        BlockHeader block = BlockHeader.parseFrom(blockAsBytes);

        for (ByteString keyString : block.getSigningPublicKeyList()) {

            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyString.toByteArray());
            KeyFactory keyFactory = KeyFactory.getInstance(getDSAKeyType());
            PublicKey publicKey = keyFactory.generatePublic(keySpec);

            Signature sig = Signature.getInstance(getDSAAlgo());
            sig.initVerify(publicKey);

            sig.update(block.toBuilder().clearHash().clearSignature().build().toByteArray());

            if (sig.verify(block.getSignature().toByteArray())) {
                return true;
            }
        }

        return false;
    }
}
