package com.shemnon.weld;

import com.google.protobuf.InvalidProtocolBufferException;
import sun.security.pkcs11.SunPKCS11;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Security;
import java.security.SignatureException;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

/**
 *
 * Created by shemnon on 2/18/16.
 */
public class YubiKeyTest extends SimpleTest {

    private SunPKCS11 p;
    private KeyPair keyPair;

    public KeyPair getKeyPair() {
        if (keyPair == null) {

            String pkcs11Config = "name = OpenSC\n" +
                "library = /Library/OpenSC/lib/opensc-pkcs11.so\n" +
                "slot = 1\n";
            ByteArrayInputStream confStream = new ByteArrayInputStream(pkcs11Config.getBytes());

            p = new SunPKCS11(confStream);
            Security.addProvider(p);
            System.out.println("Snooping");
            try {
                // Load the key store
                char[] pin = "123456".toCharArray();
                KeyStore keyStore = KeyStore.getInstance("PKCS11", p);
                keyStore.load(null, pin);

                Enumeration aliasesEnum = keyStore.aliases();

                while (aliasesEnum.hasMoreElements()) {
                    try {
                        String alias = (String) aliasesEnum.nextElement();
                        System.out.println("Alias: " + alias);
                        X509Certificate cert =
                            (X509Certificate) keyStore.getCertificate(alias);
                        cert.getPublicKey();
                        PrivateKey privateKey =
                            (PrivateKey) keyStore.getKey(alias, null);
                        keyPair = new KeyPair(cert.getPublicKey(), privateKey);
                        break;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return keyPair;
    }

    public String getHashAlgo() {
        return "SHA-256";
    }

    public String getDSAAlgo() {
        return "SHA1withRSA";
    }

    public String getDSAKeyType() {
        return "RSA";
    }

    public double[] getDifficulties() {
        return new double[] {0x2P252, 0x2P247, 0x2P242};
    }

    public static void main(String... arg) throws NoSuchAlgorithmException, InvalidKeyException,
        UnsupportedEncodingException, SignatureException, InvalidKeySpecException,
        InvalidProtocolBufferException {

        String setting = System.getProperty("java.security.debug");
        if (setting == null) {
            setting = "";
        }
        List settings = Arrays.asList(setting.split(","));
        if (!settings.contains("all") && !settings.contains("sunpkcs11")) {
            System.out.println("There's a bug in OpenSC and Yubikey.\n" +
                "The debug setting -Djava.security.debug=sunpkcs11 must be set");
        }

        new YubiKeyTest().run();
    }
}
