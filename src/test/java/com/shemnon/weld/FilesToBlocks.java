package com.shemnon.weld;

import com.google.protobuf.ByteString;
import com.shemnon.weld.Proto.BlockHeader;

import java.io.File;
import java.io.FileInputStream;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by shemnon on 3/2/16.
 */
public class FilesToBlocks {

  public static void main(String... args) throws Exception {
    File src = new File(args[0]);
    File dest = new File(args[1]);

    Map<ByteString, BlockHeader> blocks = new HashMap<>();
    Map<ByteString, ByteString> payloads = new HashMap<>();

    for (File f : src.listFiles()) {
      if (f.getName().startsWith("h_")) {
        BlockHeader bh = BlockHeader.parseFrom(new FileInputStream(f));
        if (bh.getPreviousHash().size() == 0) {
          blocks.put(ByteString.EMPTY, bh);
        } else {
          blocks.put(bh.getPreviousHash().get(0), bh);
        }
      } else if (f.getName().startsWith("d_")) {
        ByteString payload = ByteString.readFrom(new FileInputStream(f));
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.reset();
        ByteString hash = ByteString.copyFrom(md.digest(payload.toByteArray()));
        payloads.put(hash, payload);
      }
    }

    BlockFileBlockStore bfbs = new BlockFileBlockStore(null, dest);
    BlockHeader currentBlockHeader = blocks.get(ByteString.EMPTY);
    while (currentBlockHeader != null) {
      ByteString payload = currentBlockHeader.getPayload();
      if (payload != null && !ByteString.EMPTY.equals(payload)) {
        bfbs.publishData(payloads.get(currentBlockHeader.getPayload()));
      }
      bfbs.publishBlock(currentBlockHeader);
      currentBlockHeader = blocks.get(currentBlockHeader.getHash());
    }
  }
}
