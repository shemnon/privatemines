package com.shemnon.weld.github;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.shemnon.weld.BlockFileBlockStore;
import com.shemnon.weld.Proto.BlockHeader;
import com.shemnon.weld.Proto.BlockStoreEntry;
import com.shemnon.weld.Proto.PayloadEntry;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;

/**
 * Created by shemnon on 3/5/16.
 */
public class GithubDiffReporter {
  public static void main(String... args) throws IOException {
    File blockStoreDir = new File(args[0]);
    int blockHeight = -1;
    long lastTimeSec;

    int blockNum = 0;
    long nonceSum = 0;

    Table<String, String, String> gitState = HashBasedTable.create();

    File blockFile = new File(blockStoreDir, String.format(BlockFileBlockStore.FILE_NAME_FORMAT, blockNum));
    while (blockFile.isFile()) {
      FileInputStream fin = new FileInputStream(blockFile);
      PayloadEntry lastPayload = null;

      while (fin.available() > 0) {
        BlockStoreEntry bse = BlockStoreEntry.parseDelimitedFrom(fin);
        switch (bse.getContentCase()) {
          case BLOCK_HEADER:
            BlockHeader bh = bse.getBlockHeader();
            blockHeight++;
            lastTimeSec = bh.getTime();
            if (blockHeight > 0) {
              nonceSum += bh.getNonce();
            }

            if (lastPayload != null) {

              System.out.printf(
                  "Payload Change - block %d - %d nonces - at %s%n",
                  blockHeight,
                  nonceSum,
                  DateFormat.getDateTimeInstance().format(new Date(lastTimeSec*1000)));

              String payloadString = lastPayload.getContent().toString("UTF-8");
              for (String line : payloadString.split("\n+")) {
                String[] parts = line.split("\\s+");
                String current = gitState.get(parts[0], parts[1]);
                if (!parts[2].equals(current)) {
                  if (current == null) {
                    System.out.printf(
                        " %s %s%n   == %s%n",
                        parts[0],
                        parts[1],
                        parts[2]);
                  } else {
                    System.out.printf(
                        " %s %s%n      %s%n   -> %s%n",
                        parts[0],
                        parts[1],
                        current,
                        parts[2]);
                  }
                  gitState.put(parts[0], parts[1], parts[2]);
                }
              }
            }
            lastPayload = null;
            break;

          case PAYLOAD:
            lastPayload = bse.getPayload();
            break;

        }
      }


      blockNum++;
      blockFile = new File(blockStoreDir, String.format(BlockFileBlockStore.FILE_NAME_FORMAT, blockNum));
    }
  }
}
