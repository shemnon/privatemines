package com.shemnon.weld.github;

import com.google.protobuf.ByteString;
import org.kohsuke.github.GHRef;
import org.kohsuke.github.GHRepository;
import org.kohsuke.github.GitHub;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by shemnon on 2/24/16.
 */
public class GithubTest {

    public static void main(String... args) throws IOException {
        //GitHub hub = GitHub.connectAnonymously();
        GitHub hub = GitHub.connectUsingPassword("shemnon", "****");

        System.out.println(hub.getRateLimit().remaining);
        GHRepository linux = hub.getRepository("bitcoin/bitcoin");
//        for (GHRef ref : linux.getRefs()) {
//            System.out.printf("name: %s type: %s hash: %s%n", ref.getRef(), ref.getObject().getType(), ref.getObject().getSha());
//        }
        StringBuilder sb = new StringBuilder();
        sb.append(System.currentTimeMillis());
        sb.append("\n");
        new TreeMap<>(linux.getBranches()).entrySet().stream().forEach(e -> {
            sb.append(linux.getGitTransportUrl());
            sb.append("\t");
            sb.append(e.getValue().getName());
            sb.append("\t");
            sb.append(e.getValue().getSHA1());
            sb.append("\n");
        });
        System.out.println(hub.getRateLimit().remaining);
    }
}
