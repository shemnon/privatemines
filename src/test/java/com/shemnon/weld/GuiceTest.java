package com.shemnon.weld;

import com.google.common.io.Files;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import com.google.inject.TypeLiteral;
import com.google.inject.name.Names;
import com.google.protobuf.ByteString;
import com.shemnon.weld.github.MultiGithubRepoQuery;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.kohsuke.github.GitHub;

import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;

/**
 * Created by shemnon on 2/22/16.
 */
public class GuiceTest {

  static class GitWeldModule extends AbstractModule {

    @Option(name = "--block_store_dir", metaVar = "<dir>")
    File blockStoreDir = null;

    @Option(name = "--repo_name", required = true)
    List<String> repoNames = new ArrayList<>();

    @Option(name = "--github_username")
    String githubUsername;

    @Option(name = "--github_password")
    String githubPassword;

    @Option(name = "--hash")
    String hashAlgorithm = "SHA-256";

    @Option(name = "--pkcs11_lib")
    String pkcs11Lib = "/usr/lib/opensc-pkcs11.so";

    @Option(name = "--pkcs11_slot")
    String pkcs11Slot = "1";

    @Option(name = "--pkcs11_pin")
    String pkcs11PIN = "123456";

    @Option(name = "--pkcs11_dsa")
    String pkcs11DSA = "SHA1withRSA";

    @Option(name = "--pkcs11_dsa_key_type")
    String pkcs11DSAKeyType = "RSA";


    protected void configure() {
      if (blockStoreDir == null) {
        blockStoreDir = Files.createTempDir();
      }
      bind(File.class).annotatedWith(Names.named("BlockFileStoreDirectory"))
          .toInstance(blockStoreDir);

      bind(String.class).annotatedWith(Names.named("HashAlgorithm"))
          .toInstance(hashAlgorithm);
      bind(String.class).annotatedWith(Names.named("PKCS11Lib"))
          .toInstance(pkcs11Lib);
      bind(String.class).annotatedWith(Names.named("PKCS11Slot"))
          .toInstance(pkcs11Slot);
      bind(String.class).annotatedWith(Names.named("PKCS11PIN"))
          .toInstance(pkcs11PIN);
      bind(String.class).annotatedWith(Names.named("PKCS11DSA"))
          .toInstance(pkcs11DSA);
      bind(String.class).annotatedWith(Names.named("PKCS11DSAKeyType"))
          .toInstance(pkcs11DSAKeyType);

//      bind(BlockStore.class).to(InMemoryBlockStore.class).in(Singleton.class);
//      bind(BlockStore.class).toInstance(new FileSystemBlockStore(blockStoreDir));
      bind(BlockStore.class).to(BlockFileBlockStore.class).in(Singleton.class);

      bind(new TypeLiteral<Supplier<ByteString>>() {})
          .annotatedWith(Names.named("PayloadHashSupplier"))
          .to(MultiGithubRepoQuery.class);

      Collections.sort(repoNames);
      bind(new TypeLiteral<List<String>>() {})
          .annotatedWith(Names.named("SuppliedGitHubRepos"))
          .toInstance(repoNames);

      try {
        if (githubUsername != null && githubPassword != null) {
          bind(GitHub.class)
              .toInstance(
                  GitHub.connectUsingPassword(
                      githubUsername,
                      githubPassword));
        } else {
          bind(GitHub.class)
              .toInstance(GitHub.connectAnonymously());
        }
      } catch (IOException e) {
        e.printStackTrace();
      }

      bind(Welder.class).to(PKCS11Welder.class).in(Singleton.class);
//      bind(Welder.class).to(HashWelder.class).in(Singleton.class);
    }
  }

  public static void main(String... args) throws CmdLineException {
    GitWeldModule gitWeldModule = new GitWeldModule();

    CmdLineParser cmdLineParser = new CmdLineParser(gitWeldModule);
    cmdLineParser.parseArgument(args);

    Injector injector = Guice.createInjector(gitWeldModule);


    //noinspection InfiniteLoopStatement
    try (BookkeepingDevice device = injector.getInstance(BookkeepingDevice.class)) {
      device.initialize();

      //noinspection InfiniteLoopStatement
      while (true) {
        device.weldChain();
      }

    }
  }
}
