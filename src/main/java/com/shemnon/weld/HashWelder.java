package com.shemnon.weld;

import com.google.common.base.Stopwatch;
import com.google.protobuf.ByteString;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.PublicKey;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

/**
 * Created by shemnon on 2/22/16.
 */
public class HashWelder implements Welder {

  long totalNonces;
  Stopwatch stopwatch;

  ExecutorService executor = Executors.newSingleThreadExecutor();

  public HashWelder() {
    stopwatch = Stopwatch.createUnstarted();
  }

  @Override
  public PublicKey getWeldingKey() {
    return null;
  }

  @Override
  public Future<Proto.BlockHeader> weldBlock(
      Proto.BlockHeader oHeader, double difficulty, int maxNonces) {
    if (difficulty == 0) {
      difficulty = oHeader.getDifficulty();
      if (totalNonces != 0) {
        double hashRate = totalNonces * 1E9 / stopwatch.elapsed(TimeUnit.NANOSECONDS);
        double proposedDifficulty = 0x1P256 / (hashRate * 10 * 60);
        if (difficulty / proposedDifficulty > 2.0) {
          difficulty /= 2;
        } else {
          difficulty = proposedDifficulty;
        }
      }
    }
    double calculatedDifficulty = difficulty;
    return executor.submit(() -> {
      try {
        stopwatch.start();
        Optional<Proto.BlockHeader> blockOptional;
        MessageDigest md = MessageDigest.getInstance(getHashAlgorithm());
        do {
          blockOptional =
              IntStream.range(1, maxNonces)
                  .mapToObj(l -> {
                    try {
                      Proto.BlockHeader.Builder headerBuilder = oHeader.toBuilder()
                          .clearSignature()
                          .clearHash()
                          .setDifficulty(calculatedDifficulty)
                          .setNonce(l);

                      totalNonces++;

                      md.reset();
                      byte[] hash = md.digest(headerBuilder.build().toByteArray());
                      return headerBuilder.setHash(ByteString.copyFrom(hash)).build();
                    } catch (Exception e) {
                      e.printStackTrace();
                      return null;
                    }
                  }).filter(bh -> {
                double hashAsDouble = new BigInteger(bh.getHash().toByteArray())
                    .doubleValue();
                return hashAsDouble < calculatedDifficulty && hashAsDouble > 0;
              }).findFirst();
        } while (!blockOptional.isPresent());

        return blockOptional.get();
      } finally {
        stopwatch.stop();
        System.out.printf("%6.4f%n", ((double) totalNonces) / stopwatch.elapsed(TimeUnit.SECONDS));

      }
    });

  }

  @Override
  public String validateBlock(Proto.BlockHeader block) {
    try {
      MessageDigest md = MessageDigest.getInstance(getHashAlgorithm());
      Proto.BlockHeader checkHashHeader = Proto.BlockHeader.newBuilder(block)
          .clearHash()
          .build();
      md.reset();

      ByteString calculatedHash = ByteString.copyFrom(md.digest(checkHashHeader.toByteArray()));
      if (!block.getHash().equals(calculatedHash)) {
        return "Incorrect self-reported hash stored";
      }
    } catch (Exception e) {
      return e.toString();
    }
    return null;
  }

  public String getHashAlgorithm() {
    return "SHA-256";
  }
}
