package com.shemnon.weld;

import com.google.protobuf.ByteString;
import com.shemnon.weld.Proto.BlockHeader;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collections;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;

/**
 * Created by shemnon on 2/21/16.
 */
public class BookkeepingDevice implements AutoCloseable {

  @Inject
  BlockStore blockStore;

  @Inject
  Welder welder;

  @Inject
  @Named("PayloadHashSupplier")
  Supplier<ByteString> payloadHashSupplier;

  public BlockHeader weldChain() {
    BlockHeader prevBlock = blockStore.getChainHead();

    ByteString prevHash = prevBlock.getHash();

    BlockHeader.Builder currentBuilder = prevBlock.toBuilder()
        .clearHash()
        .clearSignature()
        .setPayload(payloadHashSupplier.get())
        .setTime(System.currentTimeMillis())
        .putAllPreviousHash(Collections.singletonMap(0, prevHash));

    try {
      BlockHeader newBlock = welder.weldBlock(currentBuilder.build(), 0, Integer.MAX_VALUE)
          .get();

      blockStore.publishBlock(newBlock);

      return newBlock;

    } catch (InterruptedException | ExecutionException e) {
      e.printStackTrace();
    }
    return null;
  }

  public void initialize() {
    // no-op
  }

  @Override
  public void close() {
    // no-op
  }
}
