package com.shemnon.weld;

import com.google.protobuf.ByteString;
import com.shemnon.weld.Proto.BlockHeader;

/**
 *
 * Created by shemnon on 2/21/16.
 */
public interface BlockStore {

    BlockHeader getChainHead();

    void publishBlock(BlockHeader foundBlock);

    ByteString publishData(ByteString data);

    BlockHeader getBlock(ByteString hash);
}
