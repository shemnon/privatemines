package com.shemnon.weld;

import com.google.inject.Inject;
import com.google.protobuf.ByteString;
import com.shemnon.weld.Proto.BlockHeader;

import javax.inject.Named;
import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by shemnon on 2/27/16.
 */
public class CachingBlockFileBlockStore extends BlockFileBlockStore {

  Map<ByteString, BlockHeader> headerCache;

  @Inject
  public CachingBlockFileBlockStore(Welder welder, @Named("BlockFileStoreDirectory")
      File storeDirectory) throws IOException, NoSuchAlgorithmException,
      InvalidKeySpecException, InvalidKeyException, SignatureException {
    super(welder, storeDirectory);
  }

  @Override
  public BlockHeader getBlock(ByteString hash) {
    return headerCache.get(hash);
  }

  @Override
  protected void loadingBlockHeader(BlockHeader header) {
    if (headerCache == null) {
      // this is because I'm doing some naughty constructor stuff. //FIXME
      headerCache = new HashMap<>();
    }
    headerCache.put(header.getHash(), header);
  }
}
