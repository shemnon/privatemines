package com.shemnon.weld.github;

import com.google.protobuf.ByteString;
import com.shemnon.weld.BlockStore;
import org.kohsuke.github.GHRepository;
import org.kohsuke.github.GitHub;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.List;
import java.util.TreeMap;
import java.util.function.Supplier;

/**
 * Created by shemnon on 2/26/16.
 */
public class MultiGithubRepoQuery implements Supplier<ByteString> {

    @Inject
    BlockStore blockStore;

    @Inject
    GitHub github;

    @Inject @Named("SuppliedGitHubRepos")
    List<String> githubRepos;

    @Override
    public ByteString get() {
        try {
            StringBuilder sb = new StringBuilder();
            for (String githubRepo : githubRepos) {
                GHRepository repository = github.getRepository(githubRepo);
//                for (GHRef ref : repository.getRefs()) {
//                    System.out.printf("name: %s type: %s hash: %s%n", ref.getRef(), ref.getObject()
//                        .getType(), ref.getObject().getSha());
//                }
//                sb.append(System.currentTimeMillis());
//                sb.append("\n");
                new TreeMap<>(repository.getBranches()).entrySet().stream().forEach(e -> {
                    sb.append(repository.getGitTransportUrl());
                    sb.append("\t");
                    sb.append(e.getValue().getName());
                    sb.append("\t");
                    sb.append(e.getValue().getSHA1());
                    sb.append("\n");
                });
            }
            return blockStore.publishData(ByteString.copyFrom(sb.toString().getBytes()));
        } catch (IOException e) {
            e.printStackTrace();
            return ByteString.EMPTY;
        }
    }

}
