package com.shemnon.weld;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.shemnon.weld.Proto.BlockHeader;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * Created by shemnon on 2/22/16.
 */
public class InMemoryBlockStore implements BlockStore {

    Map<ByteString, BlockHeader> memorySet = new HashMap<>();
    Map<ByteString, ByteString> dataSet = new HashMap<>();

    BlockHeader currentBlock;

    public InMemoryBlockStore() {
        BlockHeader.Builder partial = BlockHeader.newBuilder()
            .setVersion(0)
            .setDifficulty(0x1P250)
            .setNonce(0x1eafcafedecafbadL);
        try {
            MessageDigest md = MessageDigest.getInstance(getHashAlgo());
            partial.setHash(ByteString.copyFrom(md.digest(partial.build().toByteArray())));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        publishBlock(partial.build());
    }

    @Override
    public BlockHeader getChainHead() {
        return currentBlock;
    }

    @Override
    public void publishBlock(BlockHeader foundBlock) {
        System.out.println(foundBlock);
        memorySet.put(foundBlock.getHash(), foundBlock);
        currentBlock = foundBlock;
    }

    @Override
    public ByteString publishData(ByteString data) {
        try {
            MessageDigest md = MessageDigest.getInstance(getHashAlgo());
            ByteString bs = ByteString.copyFrom(md.digest(data.toByteArray()));
            dataSet.put(bs, data);
            return bs;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return ByteString.EMPTY;
        }
    }

    @Override
    public BlockHeader getBlock(ByteString hash) {
        return memorySet.get(hash);
    }

    public String getHashAlgo() {
        return "SHA-256";
    }

}
