package com.shemnon.weld;

import com.google.common.base.Stopwatch;
import com.google.protobuf.ByteString;
import com.shemnon.weld.Proto.BlockHeader;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collections;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

/**
 * Created by shemnon on 2/21/16.
 */
public class AuditingDevice implements AutoCloseable {

  @Inject
  BlockStore blockStore;

  @Inject
  Welder welder;

  @Inject
  @Named("PayloadHashSupplier")
  Supplier<ByteString> payloadHashSupplier;

  /**
   *
   * @param blockHash
   * @return Time in ms it took to calculate the block.
   */
  public long auditChain(ByteString blockHash) {
    BlockHeader thisBlock = blockStore.getBlock(blockHash);
    BlockHeader prevBlock = blockStore.getBlock(thisBlock.getPreviousHash().get(0));

    ByteString prevHash = prevBlock.getHash();

    BlockHeader.Builder currentBuilder = prevBlock.toBuilder()
        .clearHash()
        .clearSignature()
        .setPayload(thisBlock.getPayload())
        .setTime(thisBlock.getTime())
        .putAllPreviousHash(Collections.singletonMap(0, prevHash));

    try {
      Stopwatch stopwatch = Stopwatch.createStarted();
      BlockHeader newBlock = welder.weldBlock(currentBuilder.build(), thisBlock.getDifficulty(), (int)thisBlock.getNonce())
          .get();
      stopwatch.stop();

      if (newBlock.getHash().equals(thisBlock.getHash())) {
        return stopwatch.elapsed(TimeUnit.MILLISECONDS);
      } else {
        return -1;
      }

    } catch (InterruptedException | ExecutionException e) {
      e.printStackTrace();
    }
    return -1;
  }

  public void initialize() {
    // no-op
  }

  @Override
  public void close() {
    // no-op
  }
}
