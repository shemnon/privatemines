package com.shemnon.weld;

import com.google.protobuf.ByteString;
import com.google.protobuf.util.JsonFormat;
import com.shemnon.weld.Proto.BlockHeader;
import org.apache.commons.codec.binary.Hex;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by shemnon on 2/23/16.
 *
 * In this block store data and blocks each have their own file based on the hash of their content.
 *
 * headers are named h_&lt;hex&gt;.pb
 * data is named d_&lghex&gt;.pb
 *
 */
public class FileSystemBlockStore implements BlockStore {

    File rootDirectory;

    BlockHeader chainHead;

    public FileSystemBlockStore(File rootDirectory) {
        this.rootDirectory = rootDirectory;
    }

    @Override
    public BlockHeader getChainHead() {
        if (chainHead == null) {
            try {
                chainHead = findBlockTip();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return chainHead;
    }

    BlockHeader createGenesisBlock() {
        BlockHeader.Builder partial = BlockHeader.newBuilder()
            .setVersion(0)
            .setDifficulty(0x1P256 / ((1.4921 * 60 * 10)))
            .setTime(System.currentTimeMillis())
            .setNonce(0x1eafcafedecafbadL);
        try {
            MessageDigest md = MessageDigest.getInstance(getHashAlgo());
            partial.setHash(ByteString.copyFrom(md.digest(partial.build().toByteArray())));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return partial.build();
    }

    @Override
    public void publishBlock(BlockHeader foundBlock) {
        try {
            FileOutputStream fos = new FileOutputStream(new File(rootDirectory,
                "h_" + getFileName(foundBlock.getHash())));
            foundBlock.writeTo(fos);

            System.out.println(JsonFormat.printer().print(foundBlock));

            chainHead = foundBlock;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ByteString publishData(ByteString data) {
        try {
            MessageDigest md = MessageDigest.getInstance(getHashAlgo());
            ByteString bs = ByteString.copyFrom(md.digest(data.toByteArray()));
            FileOutputStream fos = new FileOutputStream(new File(rootDirectory,
                "d_" + getFileName(bs)));
            fos.write(data.toByteArray());
            return bs;
        } catch (NoSuchAlgorithmException | IOException e) {
            e.printStackTrace();
            return ByteString.EMPTY;
        }
    }

    @Override
    public BlockHeader getBlock(ByteString hash) {
        try {
            FileInputStream fis = new FileInputStream(new File(rootDirectory,
                "h_" + getFileName(hash)));
            return BlockHeader.parseFrom(fis);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    String getFileName(ByteString hash) {
        return Hex.encodeHexString(hash.toByteArray()) + ".pb";
    }

    public String getHashAlgo() {
        return "SHA-256";
    }

    public BlockHeader findBlockTip() throws IOException {
        Map<ByteString, BlockHeader> chains = new HashMap<>();
        // lets be too clever.
        // first map hashes to the blocks
        Files.walk(rootDirectory.toPath())
            .filter(path -> path.toFile().getName().startsWith("h_"))
            .map(path -> {
                try {
                    return BlockHeader.parseFrom(Files.readAllBytes(path));
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }).filter(bh -> bh != null)
            .forEach(bh -> {
                ByteString prev = bh.getPreviousHash().getOrDefault(0, ByteString.EMPTY);
                ByteString current = bh.getHash();
                chains.put(current, bh);
            });
        // now try to perfect the links
        for (BlockHeader bh : new ArrayList<>(chains.values())) {
            ByteString hash = bh.getPreviousHash().get(0);
            if (hash != null) {
                chains.remove(hash);
            }
        }
        System.out.println("Chain Tips - ");
        BlockHeader answer = null;
        for (BlockHeader bh : chains.values()) {
            System.out.println("  " + Hex.encodeHexString(bh.getHash().toByteArray()));
            answer = bh;
        }

        if (answer == null) {
            System.out.println(" - None! -- Generating Genesis Block");

            answer = createGenesisBlock();
            publishBlock(answer);
        }

        return answer;
    }
}
