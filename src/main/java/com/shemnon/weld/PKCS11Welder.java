package com.shemnon.weld;

import com.google.common.base.Stopwatch;
import com.google.protobuf.ByteString;
import sun.security.pkcs11.SunPKCS11;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.Signature;
import java.security.cert.X509Certificate;
import java.security.spec.X509EncodedKeySpec;
import java.util.Enumeration;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static com.shemnon.weld.Proto.BlockHeader;

/**
 * Created by shemnon on 2/22/16.
 */
public class PKCS11Welder implements Welder {

  private KeyPair keyPair;
  private ByteString signingKeyByteString;

  @Inject @Named("PKCS11Lib")
  String pkcs11Lib;

  @Inject @Named("PKCS11Slot")
  String pkcs11Slot;

  @Inject @Named("PKCS11PIN")
  String pkcs11PIN;

  @Inject @Named("HashAlgorithm")
  String hashAlgo;

  @Inject @Named("PKCS11DSA")
  String DSAAlgo;

  @Inject @Named("PKCS11DSAKeyType")
  String DSAKeyType;

  long totalNonces;
  Stopwatch stopwatch;

  ExecutorService executor = Executors.newSingleThreadExecutor();
  private SunPKCS11 p;

  public PKCS11Welder() {
    stopwatch = Stopwatch.createUnstarted();
  }

  private void setupProvider() {
    if (p != null) return;

    String pkcs11Config = "name = OpenSC\n" +
        "library = " + pkcs11Lib + "\n" +
        "slot = " + pkcs11Slot + "\n";
    ByteArrayInputStream confStream = new ByteArrayInputStream(pkcs11Config.getBytes());

    p = new SunPKCS11(confStream);
    Security.addProvider(p);
  }

  private void setupKeys() {
    setupProvider();
    System.out.println("Snooping");
    try {
//      AuthProvider aprov = Security.getProvider("SunPKCS11-NSS-FIPS");
//      aprov.login(subject, new MyCallbackHandler());

      // Load the key store
      char[] pin = pkcs11PIN.toCharArray();
      KeyStore keyStore = KeyStore.getInstance("PKCS11", p);
      keyStore.load(null, pin);

      Enumeration aliasesEnum = keyStore.aliases();

      while (aliasesEnum.hasMoreElements()) {
        try {
          String alias = (String) aliasesEnum.nextElement();
          System.out.println("Alias: " + alias);
          X509Certificate cert =
              (X509Certificate) keyStore.getCertificate(alias);
          cert.getPublicKey();
          PrivateKey privateKey =
              (PrivateKey) keyStore.getKey(alias, null);
          keyPair = new KeyPair(cert.getPublicKey(), privateKey);
          signingKeyByteString = ByteString.copyFrom(cert.getPublicKey().getEncoded());
          break;
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public PublicKey getWeldingKey() {
    return keyPair.getPublic();
  }

  @Override
  public Future<BlockHeader> weldBlock(BlockHeader oHeader, double difficulty, int maxNonces) {
    if (keyPair == null) {
      setupKeys();
    }
    if (difficulty == 0 && maxNonces != Integer.MAX_VALUE) {
      difficulty = oHeader.getDifficulty();
      if (totalNonces != 0) {
        double hashRate = totalNonces * 1E9 / stopwatch.elapsed(TimeUnit.NANOSECONDS);
        double proposedDifficulty = 0x1P256 / (hashRate * 10 * 60);
        if (difficulty / proposedDifficulty > 2.0) {
          difficulty /= 2;
        } else {
          difficulty = proposedDifficulty;
        }
      }
    }
    double calculatedDifficulty = difficulty;
    return executor.submit(() -> {
      try {
        stopwatch.start();
        BlockHeader resultBlock = null;
        MessageDigest md = MessageDigest.getInstance(getHashAlgorithm());
        for (int i = 1; i <= maxNonces; i++) {
            try {
              BlockHeader.Builder headerBuilder = oHeader.toBuilder()
                  .clearSignature()
                  .clearHash()
                  .clearSigningPublicKey()
                  .setDifficulty(calculatedDifficulty)
                  .addSigningPublicKey(signingKeyByteString)
                  .setNonce(i);

              PrivateKey priv = keyPair.getPrivate();

              Signature dsa = Signature.getInstance(getDSAAlgo());

              dsa.initSign(priv);

              dsa.update(headerBuilder.build().toByteArray());
              byte[] sig = dsa.sign();
              headerBuilder.setSignature(ByteString.copyFrom(sig));
              totalNonces++;

              md.reset();
              byte[] hash = md.digest(headerBuilder.build().toByteArray());
              double hashAsDouble = new BigInteger(hash).doubleValue();
              headerBuilder.setHash(ByteString.copyFrom(hash)).build();
              resultBlock = headerBuilder.build();
              if (hashAsDouble < calculatedDifficulty && maxNonces == Integer.MAX_VALUE) {
                break;
              }
            } catch (Exception e) {
              e.printStackTrace();
              return null;
            }
        }

        return resultBlock;
      } finally {
        stopwatch.stop();
        System.out.printf("%6.4f%n", ((double) totalNonces) / (stopwatch.elapsed(TimeUnit.NANOSECONDS) / 1_000_000_000.0D));
      }
    });
  }

  @Override
  public String validateBlock(BlockHeader block) {
    setupProvider();
    setupKeys();

    try {
      MessageDigest md = MessageDigest.getInstance(getHashAlgorithm());
      BlockHeader checkHashHeader = BlockHeader.newBuilder(block)
          .clearHash()
          .build();
      md.reset();

      ByteString calculatedHash = ByteString.copyFrom(md.digest(checkHashHeader.toByteArray()));
      if (!block.getHash().equals(calculatedHash)) {
        return "Incorrect self-reported hash stored";
      }

      if (block.getSigningPublicKeyCount() == 1) {
        BlockHeader checkSignatureHeader = BlockHeader.newBuilder(block)
            .clearSignature()
            .clearHash()
            .build();
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(
            block.getSigningPublicKey(0).toByteArray());
        KeyFactory keyFactory = KeyFactory.getInstance(getDSAKeyType());
        PublicKey publicKey = keyFactory.generatePublic(keySpec);

        Signature sig = Signature.getInstance(getDSAAlgo(), p);
        sig.initVerify(publicKey);

        sig.update(checkSignatureHeader.toByteArray());

        if (!sig.verify(block.getSignature().toByteArray())) {
          return "Failed signature validation.";
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      return e.toString();
    }
    return null;
  }

  public String getHashAlgorithm() {
    return hashAlgo;
  }

  public String getDSAAlgo() {
    return DSAAlgo;
  }

  public String getDSAKeyType() {
    return DSAKeyType;
  }

}
