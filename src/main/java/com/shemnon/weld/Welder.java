package com.shemnon.weld;

import com.shemnon.weld.Proto.BlockHeader;

import java.security.PublicKey;
import java.util.concurrent.Future;

/**
 * Created by shemnon on 2/21/16.
 */
public interface Welder {

  PublicKey getWeldingKey();

  Future<BlockHeader> weldBlock(BlockHeader header, double difficulty, int maxNonces);

  /**
   * Checks to see if the block is valid.  It is presumed all forms of validity are equal and
   * only invalid blocks need to be explained.
   *
   * @param block the block to be validated
   * @return null if valid, a text string representing the error if not valid.
   */
  String validateBlock(BlockHeader block);

  String getHashAlgorithm();
}
