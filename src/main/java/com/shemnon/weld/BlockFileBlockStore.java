package com.shemnon.weld;

import com.google.inject.Inject;
import com.google.protobuf.ByteString;
import com.google.protobuf.util.JsonFormat;
import com.shemnon.weld.Proto.BlockHeader;
import com.shemnon.weld.Proto.BlockStoreEntry;
import com.shemnon.weld.Proto.ChainReferenceDescription;
import com.shemnon.weld.Proto.PayloadEntry;

import javax.inject.Named;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by shemnon on 2/27/16.
 */
public class BlockFileBlockStore implements BlockStore {

  public static final String FILE_NAME_FORMAT = "blk_%06x.pb";

  static final int CONTENT_CACHE_LENGTH = 10;
  static final long FILE_SIZE_ROLLOVER = 1 << 19; // 1 MiB

  File storeDirectory;
  File currentBlock;
  int currentBlockNum = 0;
  BlockHeader chainHead = null;

  LinkedHashMap<ByteString, ByteString> dataCache = new LinkedHashMap<>();

  Welder welder;

  @Inject
  public BlockFileBlockStore(Welder welder, @Named("BlockFileStoreDirectory")
      File storeDirectory) throws IOException, NoSuchAlgorithmException,
      InvalidKeySpecException, InvalidKeyException, SignatureException {
    this.storeDirectory = storeDirectory;
    this.welder = welder;

    validateChain(true);
    if (currentBlock == null) {
      currentBlockNum = 0;
      currentBlock = new File(storeDirectory, String.format(FILE_NAME_FORMAT, currentBlockNum));
      currentBlock.getParentFile().mkdirs();
      currentBlock.createNewFile();
    }
    if (chainHead == null) {
      publishBlock(createGenesisBlock());
    }
  }

  @Override
  public BlockHeader getChainHead() {
    return chainHead;
  }

  @Override
  public void publishBlock(BlockHeader foundBlock) {
    try {
      writeToBlockFile(BlockStoreEntry.newBuilder()
          .setBlockHeader(foundBlock)
          .build());
      chainHead = foundBlock;
      System.out.println(JsonFormat.printer().print(foundBlock));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  public ByteString publishData(ByteString data) {
    ByteString hash;
    // calculate the hash
    try {
      MessageDigest md = MessageDigest.getInstance(welder.getHashAlgorithm());
      hash = ByteString.copyFrom(md.digest(data.toByteArray()));

      // only store if we are not in the past N data bits
      if (!dataCache.containsKey(hash)) {
        while (dataCache.size() >= CONTENT_CACHE_LENGTH) {
          dataCache.remove(dataCache.keySet().iterator().next());
        }
        dataCache.put(hash, hash);

        writeToBlockFile(BlockStoreEntry.newBuilder()
            .setPayload(
                PayloadEntry.newBuilder()
                    .setExpectedHash(hash)
                    .setContent(data)
                    .build())
            .build());
      }
      return hash;
    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  @Override
  public BlockHeader getBlock(ByteString hash) {
    return null;
  }

  /**
   * Rules:
   * <UL>
   * <LI>Headers must be in order.</LI>
   * <LI>When a previous block references changes, it must change to the previous block read.</LI>
   * <LI>When a payload is seen, it must be within the last 10 data enteries.</LI>
   */
  public boolean validateChain(boolean softFailure) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, SignatureException {
    Map<Integer, BlockHeader> previousBlockHeaders = new TreeMap<>();
    BlockHeader previousBlockHeader;

    LinkedHashMap<ByteString, ByteString> payloadSeen = new LinkedHashMap<>();

    currentBlockNum = -1;
    boolean success = true;
    while (true) {
      File proposedBlock = new File(storeDirectory, String.format(FILE_NAME_FORMAT,
          currentBlockNum + 1));
      if (!proposedBlock.isFile()) break;
      currentBlock = proposedBlock;

      FileInputStream fis = new FileInputStream(currentBlock);
      while (fis.available() > 0) {
        BlockStoreEntry bse = BlockStoreEntry.parseDelimitedFrom(fis);

        switch (bse.getContentCase()) {
          case BLOCK_HEADER:
            currentBlockNum++;
            chainHead = bse.getBlockHeader();
            loadingBlockHeader(chainHead);

            if (welder != null) {
              String validation = welder.validateBlock(chainHead);
              if (validation != null) {
                if (softFailure) {
                  System.err.println("Block #" + currentBlockNum + ": " + validation);
                  success = false;
                } else {
                  throw new RuntimeException("Block #" + currentBlockNum + ": " + validation);
                }
              }
            }
            // TODO validate all fields being set

            for (Map.Entry<Integer, ByteString> entry : chainHead.getPreviousHash().entrySet()) {
              Integer chainNumber = entry.getKey();
              BlockHeader bhp = previousBlockHeaders.get(chainNumber);
              if (bhp == null) {
                previousBlockHeaders.put(chainNumber, chainHead);
              } else {
                ByteString prevHash = entry.getValue();
                if (prevHash.equals(bhp.getHash())) {
                  previousBlockHeaders.put(chainNumber, chainHead);
                } else {
                  ByteString prevPrevHash = bhp.getPreviousHash().get(chainNumber);
                  if (!prevHash.equals(prevPrevHash)) {
                    String message = "Block References for " + chainNumber + " must " +
                        "either be the previous block or the same as the reference in the " +
                        "previous block";
                    if (softFailure) {
                      System.out.println(message);
                      return false;
                    } else {
                      throw new RuntimeException(message);
                    }
                  }
                }
              }
            }

            break;

          case PAYLOAD:
            PayloadEntry pe = bse.getPayload();
            //TODO validate expected hash

            // trim cache down to size
            while (payloadSeen.size() >= CONTENT_CACHE_LENGTH) {
              payloadSeen.remove(payloadSeen.keySet().iterator().next());
            }

            payloadSeen.put(pe.getExpectedHash(), pe.getExpectedHash());
            break;

          case CHAIN_REFERENCE_DESCRIPTION:
            //TODO set up metadata for validation
            ChainReferenceDescription crd = bse.getChainReferenceDescription();
            break;

          default:
            String message = "Unknown or unset block entry: " + bse.getContentCase()
                .getNumber();
            if (softFailure) {
              System.err.println(message);
              success = false;
            } else {
              throw new RuntimeException(message);
            }
        }
      }
    }
    return success;
  }

  protected void loadingBlockHeader(BlockHeader header) {
    // no-op, this is an extension point for subclasses
  }


  protected synchronized void writeToBlockFile(BlockStoreEntry entry) throws IOException {
    // verify we aren't to large, and create a new file if we are
    if (currentBlock.length() > FILE_SIZE_ROLLOVER) {
      //TODO "seal" the block file with a signature?
      currentBlock = new File(storeDirectory, String.format(FILE_NAME_FORMAT, ++currentBlockNum));
      currentBlock.createNewFile();
    }

    // write the content
    FileOutputStream fout = new FileOutputStream(currentBlock, true);
    entry.writeDelimitedTo(fout);
    fout.close();
  }

  BlockHeader createGenesisBlock() {
    BlockHeader.Builder partial = BlockHeader.newBuilder()
        .setVersion(0)
        //.setDifficulty(0x1P256 / ((1.4921 * 60 * 10)))
        .setDifficulty(0x1P256)
        .setTime(System.currentTimeMillis())
        .setNonce(0x1eafcafedecafbadL);
    try {
      MessageDigest md = MessageDigest.getInstance(welder.getHashAlgorithm());
      partial.setHash(ByteString.copyFrom(md.digest(partial.build().toByteArray())));
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }
    return partial.build();
  }
}
